package br.com.cubostest.cubos_test_app.api

import okhttp3.Interceptor
import okhttp3.Response

class TmdbInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val httpUrl = request.url

        val newUrl = httpUrl.newBuilder()
            .addQueryParameter("api_key", API_KEY)
            .addQueryParameter("language", "pt-BR")
            .build()

        val newRequest = request.newBuilder().url(newUrl).build()

        return chain.proceed(newRequest)
    }

    companion object{
        const val API_KEY = "f7a906c862b4c82a5bf72a7d0cb496d2"
    }
}