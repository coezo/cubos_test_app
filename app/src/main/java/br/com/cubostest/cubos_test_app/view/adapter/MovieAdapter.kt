package br.com.cubostest.cubos_test_app.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.R
import br.com.cubostest.cubos_test_app.model.Movie
import br.com.cubostest.cubos_test_app.util.buildPosterBaseUrl
import br.com.cubostest.cubos_test_app.view.activity.MovieDetailActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.movie_item.view.*
import kotlin.properties.Delegates

class MovieAdapter(private val context: Context, private val basePosterUrl: String) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>(){

    var movies by Delegates.observable(ArrayList<Movie>()) { _, _, _ ->
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount() = movies.size

    override fun getItemId(position: Int) = position.toLong()

    override fun getItemViewType(position: Int) = position

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]

        holder.title.text = movie.title

        val posterUrl = basePosterUrl + movie.posterPath

        Glide
            .with(context)
            .load(posterUrl)
            .into(holder.poster)

    }

    fun addItems(items: List<Movie>) {
        if(items.isNotEmpty()) {
            val position = itemCount
            movies.addAll(items)
            for(i in position until movies.size - 1){
                notifyItemInserted(i)
            }
        }
    }

    inner class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val poster: AppCompatImageView = view.poster_image
        val title: AppCompatTextView = view.movie_title

        init {
            view.setOnClickListener {
                val intent = Intent(context, MovieDetailActivity::class.java)
                intent.putExtra("movie", movies[adapterPosition])
                context.startActivity(intent)
            }
        }

    }

}