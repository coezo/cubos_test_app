package br.com.cubostest.cubos_test_app.view.activity

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.R
import br.com.cubostest.cubos_test_app.model.Genre
import br.com.cubostest.cubos_test_app.view.adapter.ViewPagerAdapter
import br.com.cubostest.cubos_test_app.view.fragment.GenreFragment
import br.com.cubostest.cubos_test_app.viewmodel.ConfigurationViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var configurationViewModel = ConfigurationViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        supportActionBar!!.elevation = 0.0f
        supportActionBar!!.title = getString(R.string.movies)

        prepareObservers()

        getConfiguration()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        (menu!!.findItem(R.id.action_search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }

        return true
    }

    private fun prepareObservers(){
        observeConfiguration()
        observeError()
    }

    private fun observeConfiguration(){
        configurationViewModel.configuration.observe(this, Observer {
            (application as CubosApplication).configuration = it
            val imC = it.imagesConfig
            setupTabs()
        })
    }

    private fun observeError() {
        configurationViewModel.error.observe(this, Observer {
            Log.e("Error", it)
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun getConfiguration(){
        configurationViewModel.getConfiguration(applicationContext)
    }

    private fun setupTabs(){
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(GenreFragment(Genre.ACAO.id), getString(R.string.acao))
        adapter.addFragment(GenreFragment(Genre.DRAMA.id), getString(R.string.drama))
        adapter.addFragment(GenreFragment(Genre.FANTASIA.id), getString(R.string.fantasia))
        adapter.addFragment(GenreFragment(Genre.FICCAO.id), getString(R.string.ficcao))
        viewPager.adapter = adapter
        tabs.setupWithViewPager(viewPager)
    }
}