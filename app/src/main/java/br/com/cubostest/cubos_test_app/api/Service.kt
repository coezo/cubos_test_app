package br.com.cubostest.cubos_test_app.api

import br.com.cubostest.cubos_test_app.model.Configuration
import br.com.cubostest.cubos_test_app.model.MovieSearchInfo
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Service {

    @GET("configuration")
    fun getConfiguration(): Observable<Response<Configuration>>

    @GET("discover/movie")
    fun getMoviesByGenre(
        @Query("with_genres") genreId: Int,
        @Query("page") page: Int
    ): Observable<Response<MovieSearchInfo>>

    @GET("search/movie")
    fun searchMovies(
        @Query("query") query: String,
        @Query("page") page: Int
    ): Observable<Response<MovieSearchInfo>>
}