package br.com.cubostest.cubos_test_app.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.R
import br.com.cubostest.cubos_test_app.model.Movie
import br.com.cubostest.cubos_test_app.util.buildBackdropBaseUrl
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setContentView(R.layout.activity_movie_detail)

        val movie = intent.getSerializableExtra("movie") as Movie

        supportActionBar!!.title = movie.title

        val posterUrl = buildBackdropBaseUrl((application as CubosApplication).configuration) + movie.backdropPath

        Glide
            .with(this)
            .load(posterUrl)
            .into(poster_image)

        description.text = movie.summary

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

}