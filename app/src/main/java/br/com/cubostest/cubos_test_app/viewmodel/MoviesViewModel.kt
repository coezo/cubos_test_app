package br.com.cubostest.cubos_test_app.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.model.MovieSearchInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MoviesViewModel : ViewModel() {

    private var disposables = CompositeDisposable()

    private var status = MutableLiveData<Boolean>()
    var error = MutableLiveData<String>()

    var movies = MutableLiveData<MovieSearchInfo>()

    fun getMoviesByGenre(context: Context, genreId: Int){
        val page = (movies.value?.page ?: 0) + 1
        Log.d("Searching movies", "Loading page $page")

        disposables.add((context.applicationContext as CubosApplication).getService().getMoviesByGenre(genreId, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{ status.value = true }
            .doFinally { status.value = false }
            .subscribe({
                movies.value = it.body()
            }, {
                error.value = it.localizedMessage
            })
        )
    }

    fun searchMovies(context: Context, query: String){
        val page = (movies.value?.page ?: 0) + 1
        Log.d("Searching movies", "Loading page $page")

        disposables.add((context.applicationContext as CubosApplication).getService().searchMovies(query, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{ status.value = true }
            .doFinally { status.value = false }
            .subscribe({
                movies.value = it.body()
            }, {
                error.value = it.localizedMessage
            })
        )
    }

}