package br.com.cubostest.cubos_test_app.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Movie (
    @SerializedName("title")
    var title: String,
    @SerializedName("overview")
    var summary: String,
    @SerializedName("poster_path")
    var posterPath: String,
    @SerializedName("backdrop_path")
    var backdropPath: String
) : Serializable

// This could be requested from the API instead
enum class Genre(val id: Int){
    ACAO(28),
    DRAMA(18),
    FANTASIA(14),
    FICCAO(878)
}