package br.com.cubostest.cubos_test_app.util

import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.model.Configuration

// Poster Size = "w342"
fun buildPosterBaseUrl(configuration: Configuration) = configuration.imagesConfig.baseUrl +
        configuration.imagesConfig.posterSizes[3]

// Backdrop Size = "w780"
fun buildBackdropBaseUrl(configuration: Configuration) = configuration.imagesConfig.baseUrl +
        configuration.imagesConfig.backdropSizes[1]