package br.com.cubostest.cubos_test_app.view.activity

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.R
import br.com.cubostest.cubos_test_app.model.Movie
import br.com.cubostest.cubos_test_app.util.buildPosterBaseUrl
import br.com.cubostest.cubos_test_app.view.adapter.MovieAdapter
import br.com.cubostest.cubos_test_app.viewmodel.MoviesViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_genre_tab.*

class MovieSearchActivity : AppCompatActivity() {

    private lateinit var adapter: MovieAdapter

    private val moviesViewModel = MoviesViewModel()

    private var movies = ArrayList<Movie>()

    private var canLoadPage = false

    private lateinit var query: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setContentView(R.layout.activity_movie_search)

        adapter = MovieAdapter(this, buildPosterBaseUrl((application as CubosApplication).configuration))

        prepareRecycler()

        prepareObservers()

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            query = intent.getStringExtra(SearchManager.QUERY)
            supportActionBar!!.title = query
            searchMovies()
        }
    }


    private fun prepareRecycler() {
        recycler.layoutManager = GridLayoutManager(this, 2)
        recycler.adapter = adapter

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if(!recyclerView.canScrollVertically(1) && canLoadPage) {
                    searchMovies()
                }
            }
        })

        recycler.setRecyclerListener {
            val holder = it as MovieAdapter.MovieViewHolder
            Glide.with(holder.poster.context).clear(holder.poster)
        }
    }

    private fun prepareObservers(){
        observeMovieSearch()
        observeError()
    }

    private fun observeMovieSearch(){
        moviesViewModel.movies.observe(this, Observer {
            movies = ArrayList(it.movies)
            adapter.addItems(movies)
            canLoadPage = true
            progress.visibility = View.INVISIBLE
        })
    }

    private fun observeError() {
        moviesViewModel.error.observe(this, Observer {
            Log.e("Error", it)
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun searchMovies(){
        progress.visibility = View.VISIBLE
        canLoadPage = false
        moviesViewModel.searchMovies(this, query)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}