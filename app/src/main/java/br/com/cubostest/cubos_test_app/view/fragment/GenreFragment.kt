package br.com.cubostest.cubos_test_app.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.cubostest.cubos_test_app.CubosApplication
import br.com.cubostest.cubos_test_app.R
import br.com.cubostest.cubos_test_app.model.Genre
import br.com.cubostest.cubos_test_app.model.Movie
import br.com.cubostest.cubos_test_app.util.buildPosterBaseUrl
import br.com.cubostest.cubos_test_app.view.adapter.MovieAdapter
import br.com.cubostest.cubos_test_app.viewmodel.MoviesViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_genre_tab.*


class GenreFragment(val genreId: Int) : Fragment() {

    private lateinit var adapter: MovieAdapter

    private val moviesViewModel = MoviesViewModel()

    private var movies = ArrayList<Movie>()

    private var canLoadPage = false

    override fun onCreateView(
        inflater: LayoutInflater,
        viewGroup: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_genre_tab, viewGroup, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = MovieAdapter(context!!, buildPosterBaseUrl((activity!!.application as CubosApplication).configuration))

        prepareRecycler()

        prepareObservers()

        searchMovies()
    }

    private fun prepareRecycler() {
        recycler.layoutManager = GridLayoutManager(activity, 2)
        recycler.adapter = adapter

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if(!recyclerView.canScrollVertically(1) && canLoadPage) {
                    searchMovies()
                }
            }
        })

        recycler.setRecyclerListener {
            val holder = it as MovieAdapter.MovieViewHolder
            Glide.with(holder.poster.context).clear(holder.poster)
        }
    }

    private fun prepareObservers(){
        observeMovieSearch()
        observeError()
    }

    private fun observeMovieSearch(){
        moviesViewModel.movies.observe(this, Observer {
            movies = ArrayList(it.movies)
            adapter.addItems(movies)
            canLoadPage = true
            progress.visibility = View.INVISIBLE
        })
    }

    private fun observeError() {
        moviesViewModel.error.observe(this, Observer {
            Log.e("Error", it)
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun searchMovies(){
        progress.visibility = View.VISIBLE
        canLoadPage = false
        moviesViewModel.getMoviesByGenre(context!!, genreId)
    }

}